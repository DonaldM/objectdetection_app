import numpy as np
import cv2
import face_recognition

import os
from datetime import datetime

path = "/home/donald/project/computer_vision/face_DR/images/db_images"
path_file = "/home/donald/project/computer_vision/face_DR"
images = []
names = []
images_list = os.listdir(path)
print(images_list)

for img in images_list:
    image = cv2.imread(os.path.join(path, img))
    images.append(image)
    names.append(img.split(".")[0].replace("_", " "))

print(names)

def encode_img(images:list):
    encodings = []
    for img in images :
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        encodings.append(face_recognition.face_encodings(img)[0])
    return encodings

def mark_attendance(name):
    with open(os.path.join(path_file, "attendance.csv"), "r+") as f:
        data_list = f.readlines()
        name_attend = []
        for line in data_list:
            entry = line.split(',')
            name_attend.append(entry[0])
            
        if name not in name_attend:
            now = datetime.now()
            datestring = now.strftime("%H:%M:%S")
            f.writelines(f'\n{name}, {datestring}')


encodings_faces = encode_img(images)
print("Encoding complete")

cap = cv2.VideoCapture(0)
while True:
    success, img = cap.read()
    imgsmall = cv2.resize(img, (0, 0), None, 0.25, 0.25)
    imgsmall = cv2.cvtColor(imgsmall, cv2.COLOR_BGR2RGB)
    
    facesloca_imgsmall = face_recognition.face_locations(imgsmall)
    encoding_imgsmall = face_recognition.face_encodings(imgsmall, facesloca_imgsmall)

    for encode_face, facelocation in zip(encoding_imgsmall, facesloca_imgsmall):
        matches = face_recognition.compare_faces(encodings_faces, encode_face)
        face_dist = face_recognition.face_distance(encodings_faces, encode_face)
        #print(face_dist)
        match_index = np.argmin(face_dist)

        if matches[match_index]:
            name_good = names[match_index].upper()
            #print(name_good)
            y1, x2, y2, x1 = facelocation
            y1, x2, y2, x1 = y1*4, x2*4, y2*4, x1*4
            cv2.rectangle(img, (x1, y1), (x2, y2), (0,255,0), 2)
            cv2.rectangle(img, (x1, y2-35), (x2, y2), (0,255,0), cv2.FILLED)
            cv2.putText(img, name_good, (x1+6, y2-6), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
            mark_attendance(name_good)

    cv2.imshow("Video", img)
    if cv2.waitKey(1) == ord('q'):
        break
